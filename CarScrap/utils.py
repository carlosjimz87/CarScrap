from datetime import datetime


def compare_links(link1, link2):
    i1 = link1.index("?id=")
    i2 = link2.index("?id=")
    link1 = link1[:i1]
    link2 = link2[:i2]
    if link1.strip() == link2.strip():
        return True
    else:
        return False



def print_json(carstr, filename):
    file = open(filename, "w")
    file.write(carstr + "\n")
    file.close()


def print_csv(cars, filename):
    file = open(filename, "a")
    for car in cars:
        file.write(car.printCSVLine(';') + "\n")
    file.close()


def get_elapsed_time(start_time):
    td = datetime.now() - start_time
    days = td.days
    hours = td.seconds // 3600
    minutes = td.seconds % 3600 // 60
    seconds = td.seconds % 60

    if days != 0:
        days_str = "{0} days, "
    else:
        days_str = ""

    if hours != 0:
        hours_str = "{1} hours, "
    else:
        hours_str = ""

    if minutes != 0:
        minutes_str = "{2} minutes, "
    else:
        minutes_str = ""

    seconds_str = "{3} seconds"

    fmt = days_str + hours_str + minutes_str + seconds_str
    return fmt.format(days, hours, minutes, seconds)


def generate_superID(pageID, carID):
    return abs(hash(str(carID) + str(pageID))) % (10 ** 8)

def get_features():
    dfeatures = {}
    dfeatures["Precio"] = "price"
    dfeatures["Año"] = "year"
    dfeatures["Potencia"] = "power"
    dfeatures["Kilómetros"] = "kms"
    dfeatures["Combustible"] = "fuel"
    dfeatures["Puertas"] = "doors"
    dfeatures["Cambio"] = "transmission"
    dfeatures["Emisiones"] = "emissions"
    dfeatures["Vendedor"] = "seller"
    dfeatures["Garantía"] = "warranty"
    dfeatures["Color exterior"] = "color"
    dfeatures["Capacidad maletero"] = "trunk"
    dfeatures["Longitud"] = "length"
    dfeatures["Altura"] = "height"
    dfeatures["Anchura"] = "width"
    dfeatures["Puertas"] = "doors"
    dfeatures["Plazas"] = "seats"
    dfeatures["Depósito"] = "deposit_size"
    dfeatures["Peso"] = "weight"
    dfeatures["Carrocería"] = "body_style"
    dfeatures["Velocidad máxima"] = "max_speed"
    dfeatures["Consumo combinado"] = "combined_consume"
    dfeatures["Consumo urbano"] = "urban_consume"
    dfeatures["Extraurbano"] = "rural_consume"
    dfeatures["Aceleración 0-100"] = "acceleration0to100"
    dfeatures["Autonomía"] = "autonomy"
    dfeatures["Emisión co2"] = "emissions"
    dfeatures["Potencia"] = "power"
    dfeatures["Cilindrada"] = "displacement"
    dfeatures["Cilindros"] = "cylinders"
    dfeatures["Par máximo"] = "max_pairs"
    dfeatures["Marchas"] = "speeds"
    dfeatures["Transmisión"] = "transmission"
    dfeatures["Tracción"] = "drive_position"
    return dfeatures