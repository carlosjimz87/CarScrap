#!/usr/bin/python
# -*- coding: utf8 -*-
import os
import logging

from twisted.internet import reactor, defer
from scrapy.crawler import CrawlerRunner
from scrapy.utils.log import configure_logging
from scrapy.utils.project import get_project_settings

from spiders.onecar import OneCarSpider
from spiders.cars import CarsSpider
from database import *
from utils import *


PATH = os.path.dirname(os.path.realpath(__file__))

logging.basicConfig(filename=PATH + '/crawl.log', filemode='a',
                    format='%(asctime)s - %(name)s - %(levelname)s - %(lineno)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)


def main():
    configure_logging()
    runner = CrawlerRunner(get_project_settings())

    manager = DBManager(False)

    @defer.inlineCallbacks
    def crawl():
        yield runner.crawl(CarsSpider)

        cars = manager.selectSimpleCarALL()

        for car in cars:
            link = car['link']
            carByLink = manager.selectFullCarByLink(link)
            if carByLink is None:
                yield runner.crawl(OneCarSpider, link)
            else:
                if compare_links(carByLink['link'], link) is False:
                    yield runner.crawl(OneCarSpider, link)

        reactor.stop()

    crawl()
    reactor.run()


if __name__ == '__main__':
    main()