from bs4 import BeautifulSoup
from scrapy.spiders import Spider
from items import CarItem
from utils import *
from database import *


class OneCarSpider(Spider):
    name = "onecar"
    allowed_domains = ["coches.com"]
    start_urls = []
    pageID = 1
    carID = 1
    start_time = 0
    dbmanager = None
    item = None

    def __init__(self, base_url=None, *args, **kwargs):
        if base_url is not None:
            self.start_urls = [base_url]
            self.dbmanager = DBManager(False)

        super(Spider, self).__init__(*args, **kwargs)

    def parse(self, response):
        item_invalid = False
        if len(response.text):
            self.item = CarItem()
            car = self.dbmanager.selectSimpleCarByLink(self.start_urls[0])
            self.item.setCar(car['pageID'], car['carID'], car['link'])
            soup = None
            data = None
            print("************************ PROCESSING ITEM (Pag:"+str(car['pageID']+" - Num:"+str(car['carID'])+ ")******************************"))

            try:
                soup = BeautifulSoup(response.text, "lxml")
                data = soup.find_all("h1", {"class": "cc_model_price"})

            except Exception as e:
                item_invalid = True
                print(str(e))
                pass


            #print("**************************************************************************************************************")
            try:
                if not item_invalid:
                    splitted = data[0].text.split()
                    self.item['brand'] = splitted[0].strip()
                    self.item['sbrand'] = splitted[1].strip()
                    splitted.remove(self.item['brand'])
                    splitted.remove(self.item['sbrand'])
                    ind_en = splitted.index("en")

                    model = ""
                    i = 0
                    for s in splitted:
                        if i == ind_en:
                            break
                        model += s + " "
                        i += 1
                    self.item['model'] = model.strip()
                    city = ""
                    i = 0
                    for s in splitted:
                        if i == ind_en + 1:
                            city = s
                        i += 1
                    self.item['city'] = city.strip()

            except Exception as e:
                item_invalid = True
                print(str(e))
                pass

            #print("**************************************************************************************************************")

            try:
                if not item_invalid:
                    data = soup.findAll("div", {"id": "carCompleteData"})
                    rows = data[0].findAll("div", {"class": "row"})

                    for row in rows:

                        if "<strong>" in str(row) and "</strong>" in str(row) and "<small>" in str(row) and "</small>" in str(row) and "hidden" not in str(row):
                            features = row.find_all("strong")
                            sfeatures = row.find_all("small")

                            for i in range(0, len(features)):
                                sf = sfeatures[i].text
                                f = features[i].text
                                df = get_features().get(sf)
                                if df is not None:
                                    self.item[df] = f.strip()

            except Exception as e:
                item_invalid = True
                print(str(e))
                pass


            #print("**************************************************************************************************************")

            # try:
            #     if not item_invalid:
            #         data = soup.findAll("div", {"id": "carCompleteData"})
            #         divs = data[0].findAll("div", {"id": "Descripcion"})
            #
            #         for div in divs:
            #             desc = div.findAll("span",{"class": "hidden-xs"})
            #             if len(str(desc))>0:
            #                 self.item['description'] = desc.text
            #
            # except Exception as e:
            #     print(str(e))
            #     pass

            #print("**************************************************************************************************************")

            # try:
            #     if not item_invalid:
            #         data = soup.findAll("div", {"id": "carCompleteData"})
            #         equipment = data[0].findAll("div", {"id": "textoDeSerie_extrasData"})
            #         if len(str(equipment))>0:
            #             self.item['equipment'] = equipment
            #
            # except Exception as e:
            #     print(str(e))
            #     pass

            #print("**************************************************************************************************************")

            if not item_invalid:
                self.dbmanager.insertFullCar(self.item)
                print("************************ SAVED ITEM (Pag:" + str(car['pageID'] + " - Num:" + str(car['carID']) + ")******************************"))


