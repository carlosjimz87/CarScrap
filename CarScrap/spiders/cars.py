from bs4 import BeautifulSoup
from scrapy.spiders import Spider
from items import CarItem
from database import *
from utils import *
from scrapy import Request


class CarsSpider(Spider):
    base_url = "https://www.coches.com/coches-segunda-mano/coches-ocasion.htm"
    name = "cars"
    allowed_domains = ["coches.com"]
    start_urls = [base_url]
    pageID = 0
    carID = 1
    start_time = 0
    dbmanager = None
    item = None

    def __init__(self, *args, **kwargs):
        super(Spider, self).__init__(*args, **kwargs)
        self.start_time = datetime.now()
        self.dbmanager = DBManager(False)

    def parse(self, response):
        self.pageID = self.pageID + 1
        item_invalid = False
        soup = BeautifulSoup(response.text, "lxml")
        data = soup.find_all("div", {"class": "oferta"})

        for dat in data:

            self.item = CarItem()
            link = ""
            try:
                links = dat.find_all("a", href=True)
                if len(links) > 0:
                    link = str(links[0]['href']).strip()

                self.item.setCar(self.pageID, self.carID, link)
            except Exception as e:
                print(str(e))
                item_invalid = True
                pass

            if not item_invalid:
                self.dbmanager.insertSimpleCar(self.item)
                self.carID = self.carID + 1

        buttons = soup.select("li.btn-pag > a")
        next_url = ""
        for btn in buttons:
            if str(btn).find("Siguiente") != -1 and str(btn).find("disabled") == -1:
                next_url = btn['href']

        if next_url and self.pageID < 2:
            yield Request(url=next_url, callback=self.parse, meta={'dont_merge_cookies': True})
