from scrapy import Item, Field
from utils import *


class CarItem(Item):
    # define the fields for your item here like:
    superID = Field()
    carID = Field()
    brand = Field()
    sbrand = Field()
    model = Field()
    price = Field()
    fuel = Field()
    power = Field()
    city = Field()
    link = Field()
    kms = Field()
    doors = Field()
    emissions = Field()
    seller = Field()
    color = Field()
    warranty = Field()
    year = Field()
    pageID = Field()
    trunk = Field()
    length = Field()
    height = Field()
    width = Field()
    seats = Field()
    deposit_size = Field()
    weight = Field()
    body_style = Field()
    max_speed = Field()
    combined_consume = Field()
    urban_consume = Field()
    rural_consume = Field()
    acceleration0to100 = Field()
    autonomy = Field()
    cylinders = Field()
    displacement = Field()
    max_pairs = Field()
    speeds = Field()
    transmission = Field()
    drive_position = Field()
    description = Field()
    equipment = Field()

    def setCar(self, pageID, carID, link):
        try:
            self['superID'] = generate_superID(pageID, carID)
            self['carID'] = carID
            self['pageID'] = pageID
            self['link'] = link
            self['brand'] = ""
            self['sbrand'] = ""
            self['model'] = ""
            self['price'] = ""
            self['fuel'] = ""
            self['power'] = ""
            self['city'] = ""
            self['kms'] = ""
            self['doors'] = ""
            self['emissions'] = ""
            self['seller'] = ""
            self['color'] = ""
            self['warranty'] = ""
            self['year'] = ""
            self['trunk'] = ""
            self['length'] = ""
            self['height'] = ""
            self['width'] = ""
            self['seats'] = ""
            self['deposit_size'] = ""
            self['weight'] = ""
            self['body_style'] = ""
            self['max_speed'] = ""
            self['combined_consume'] = ""
            self['rural_consume'] = ""
            self['urban_consume'] = ""
            self['acceleration0to100'] = ""
            self['autonomy'] = ""
            self['cylinders'] = ""
            self['displacement'] = ""
            self['max_pairs'] = ""
            self['speeds'] = ""
            self['transmission'] = ""
            self['drive_position'] = ""
            self['description'] = ""
            self['equipment'] = ""
        except Exception as e:
            print(str(e))
            pass


    def isFilled(self):
        if len(str(self['brand'])) > 0 and len(str(self['link'])) > 0:
            return True
        else:
            return False

    def printJson(self):
        _carID = str(self['carID'])
        _pageID = str(self['pageID'])
        _brand = str(self['brand'])
        _sbrand = str(self['sbrand'])
        _model = str(self['model'])
        _price = str(self['price'])
        _city = str(self['city'])
        _power = str(self['power'])
        _fuel = str(self['fuel'])
        _kms = str(self['kms'])
        _doors = str(self['doors'])
        _emissions = str(self['emissions'])
        _seller = str(self['seller'])
        _color = str(self['color'])
        _warranty = str(self['warranty'])
        _year = str(self['year'])
        _link = str(self['link'])
        _trunk = str(self['trunk'])
        _length = str(self['length'])
        _height = str(self['height'])
        _width = str(self['width'])
        _seats = str(self['seats'])
        _deposit_size = str(self['deposit_size'])
        _weight = str(self['weight'])
        _body_style = str(self['body_style'])
        _max_speed = str(self['max_speed'])
        _combined_consume = str(self['combined_consume'])
        _rural_consume = str(self['rural_consume'])
        _urban_consume = str(self['urban_consume'])
        _acceleration0to100 = str(self['acceleration0to100'])
        _autonomy = str(self['autonomy'])
        _cylinders = str(self['cylinders'])
        _displacement = str(self['displacement'])
        _max_pairs = str(self['max_pairs'])
        _speeds = str(self['speeds'])
        _drive_position = str(self['drive_position'])
        _description = str(self['description'])
        _equipment = str(self['equipment'])

        retstr = "{\n"
        retstr += "'CarID':'"+_carID+"',\n"
        retstr += "'PageID':'"+_pageID+"',\n"
        retstr += "'Brand':'"+_brand+"',\n"
        retstr += "'SubBrand':'"+_sbrand+"',\n"
        retstr += "'Model':'"+_model+"',\n"
        retstr += "'Price':'"+_price+"',\n"
        retstr += "'City':'"+_city+"',\n"
        retstr += "'Power':'"+_power+"',\n"
        retstr += "'Fuel':'"+_fuel+"',\n"
        retstr += "'Kms':'"+_kms+"',\n"
        retstr += "'Num. Doors':'"+_doors+"',\n"
        retstr += "'Emissions':'"+_emissions+"',\n"
        retstr += "'Seller':'"+_seller+"',\n"
        retstr += "'Color':'"+_color+"',\n"
        retstr += "'Warranty':'"+_warranty+"',\n"
        retstr += "'Year':'"+_year+"',\n"
        retstr += "'Trunk Size':'"+_trunk+"',\n"
        retstr += "'Length':'"+_length+"',\n"
        retstr += "'Height':'"+_height+"',\n"
        retstr += "'Width':'"+_width+"',\n"
        retstr += "'Weight':'"+_weight+"',\n"
        retstr += "'Num. Seats':'"+_seats+"',\n"
        retstr += "'Deposit Size':'"+_deposit_size+"',\n"
        retstr += "'Body Style':'"+_body_style+"',\n"
        retstr += "'Max Speed':'"+_max_speed+"',\n"
        retstr += "'Combined Consume':'"+_combined_consume+"',\n"
        retstr += "'Urban Consume':'"+_urban_consume+"',\n"
        retstr += "'Rural Consume':'"+_rural_consume+"',\n"
        retstr += "'Acceleration 0 to 100':'"+_acceleration0to100+"',\n"
        retstr += "'Autonomy':'"+_autonomy+"',\n"
        retstr += "'Cylinders':'"+_cylinders+"',\n"
        retstr += "'Displacement':'"+_displacement+"',\n"
        retstr += "'Max Pairs':'"+_max_pairs+"',\n"
        retstr += "'Num. Speeds':'"+_speeds+"',\n"
        retstr += "'Drive Position':'"+_drive_position+"',\n"
        retstr += "'Description':'"+_description+"',\n"
        retstr += "'Equipment':'"+_equipment+"',\n"
        retstr += "'Link':'"+_link+"',\n"
        retstr += "}\n"
        return retstr

    def printCSVLine(self, d):

        _carID = str(self['carID'])
        _pageID = str(self['pageID'])
        _brand = str(self['brand'])
        _sbrand = str(self['sbrand'])
        _model = str(self['model'])
        _price = str(self['price'])
        _city = str(self['city'])
        _power = str(self['power'])
        _fuel = str(self['fuel'])
        _kms = str(self['kms'])
        _doors = str(self['doors'])
        _emissions = str(self['emissions'])
        _seller = str(self['seller'])
        _color = str(self['color'])
        _warranty = str(self['warranty'])
        _year = str(self['year'])
        _link = str(self['link'])
        _trunk = str(self['trunk'])
        _length = str(self['length'])
        _height = str(self['height'])
        _width = str(self['width'])
        _seats = str(self['seats'])
        _deposit_size = str(self['deposit_size'])
        _weight = str(self['weight'])
        _body_style = str(self['body_style'])
        _max_speed = str(self['max_speed'])
        _combined_consume = str(self['combined_consume'])
        _rural_consume = str(self['rural_consume'])
        _urban_consume = str(self['urban_consume'])
        _acceleration0to100 = str(self['acceleration0to100'])
        _autonomy = str(self['autonomy'])
        _cylinders = str(self['cylinders'])
        _displacement = str(self['displacement'])
        _max_pairs = str(self['max_pairs'])
        _speeds = str(self['speeds'])
        _drive_position = str(self['drive_position'])
        _description = str(self['description'])
        _equipment = str(self['equipment'])

        return _pageID + d + _carID + d + _brand + d + _sbrand + d + _model + d + _price + d + _city + d + _year + d +\
               _power + d + _fuel + d + _kms + d + _doors + d + _emissions + d + _seller + d + _color + d +\
               _warranty + d + _trunk + d + _length + d + _weight + d + _seats + d + _width + d + _height + d + _deposit_size + d +\
                _body_style + d + _max_speed + d + _combined_consume + d + _body_style + d + _max_speed + d + _rural_consume + d +\
                _urban_consume + d + _acceleration0to100 + d + _autonomy + d + _cylinders + d + _displacement + d + _max_pairs + d + _speeds + d +\
                _drive_position + d + _description + d + _equipment + d + _link

    pass


