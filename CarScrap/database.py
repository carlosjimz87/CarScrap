import sqlite3
from os import remove,path
from items import CarItem
from utils import *


class DBManager:

    conn = None
    DATABASE_FILENAME = "cars.db"

    def __init__(self, rewrite):

        if rewrite is True:
            self.eliminate()
            self.create()
            return
        else:
            if self.is_created() is not True:
                self.create()
 
    def is_created(self):
        return path.isfile(self.DATABASE_FILENAME)

    def eliminate(self):
        try:
            remove(self.DATABASE_FILENAME)
        except Exception as e:
            print(str(e))
            return

    def create(self):
        try:
            self.conn = self.connect(self.DATABASE_FILENAME, sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES)
            c = self.conn.cursor()

            c.execute('''CREATE TABLE simplecars (superID INTEGER NOT NULL, pageID text NOT NULL, carID text NOT NULL, link text PRIMARY KEY)''')

            c.execute('''CREATE TABLE fullcars (superID INTEGER NOT NULL, pageID text , carID text, brand text,
                        sbrand text, model text, price text, fuel text, power text, city text, kms text, doors text,
                         emissions text, seller text, color text, warranty text, year text, trunk text, length text,
                          height text, width text, seats text, deposit_size text, weight text, body_style text, max_speed text,
                           combined_consume text, urban_consume text, rural_consume text, acceleration0to100 text, autonomy text,
                            cylinders text, displacement text, max_pairs text, speeds text, transmission text, drive_position text,
                             description text,equipment text, link text PRIMARY KEY)''')
            self.conn.close()

        except Exception as e:
            print(str(e))
            pass

    def connect(self, filename, d_types):
        return sqlite3.connect(filename, detect_types=d_types)

    def check(self):
        try:
            self.conn = self.connect(self.DATABASE_FILENAME, sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES)
            c = self.conn.cursor()

            c.execute("select name from sqlite_master where type = 'table'")

            tables = c.fetchall()

            self.conn.close()

            return True


        except Exception as e:
            print(str(e))
            return False

    def insertSimpleCar(self, car):
        if car is not None:
            try:

                self.conn = self.connect(self.DATABASE_FILENAME, sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES)
                c = self.conn.cursor()


                c.execute('''INSERT INTO simplecars(superID, pageID, carID, link)
                            SELECT * FROM (SELECT ?,?,?,?) as tmp
                              WHERE NOT EXISTS ( SELECT link FROM simplecars WHERE link=?) LIMIT 1''',
                              (car['superID'], car['pageID'], car['carID'], car['link'],car['link'],))

                self.conn.commit()
                self.conn.close()

            except Exception as e:
                print(str(e))
                pass

    def insertFullCar(self, car):
        try:

            self.conn = self.connect(self.DATABASE_FILENAME, sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES)
            c = self.conn.cursor()

            c.execute('''INSERT INTO fullcars(superID, pageID, carID,brand, sbrand, model,price, city, power,
            fuel, kms, doors,emissions, seller, color,warranty, year, trunk, length, height, width,weight,seats,
            deposit_size, body_style,max_speed, combined_consume, urban_consume, rural_consume, acceleration0to100,
            transmission, autonomy, cylinders, displacement, speeds, max_pairs,drive_position,description,equipment, link)
            SELECT * FROM (SELECT ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) as tmp 
            WHERE NOT EXISTS ( SELECT link FROM fullcars WHERE link=?) LIMIT 1''',
                      (car['superID'], car['pageID'], car['carID'], car['brand'], car['sbrand'], car['model'],
                       car['price'], car['city'], car['power'], car['fuel'], car['kms'], car['doors'],
                       car['emissions'], car['seller'], car['color'], car['warranty'], car['year'],
                       car['trunk'], car['length'], car['height'], car['width'], car['weight'], car['seats'],
                       car['deposit_size'],
                       car['body_style'], car['max_speed'], car['combined_consume'], car['urban_consume'],
                       car['rural_consume'], car['acceleration0to100'], car['transmission'], car['autonomy'],
                       car['cylinders'],
                       car['displacement'], car['speeds'], car['max_pairs'], car['drive_position'], car['description'],
                       car['equipment'], car['link'],car['link'],))

            # c.execute('''INSERT INTO fullcars(superID, pageID, carID,brand, sbrand, model,price, city, power,
            #                               fuel, kms, doors,emissions, seller, color,warranty, year, trunk,
            #                               length, height, width,weight,seats, deposit_size, body_style,max_speed,
            #                               combined_consume, urban_consume, rural_consume, acceleration0to100,transmission,
            #                               autonomy, cylinders, displacement, speeds, max_pairs,drive_position,description,equipment, link)
            #               VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)''',
            #               (car['superID'], car['pageID'], car['carID'], car['brand'],car['sbrand'],car['model'],
            #               car['price'],car['city'],car['power'],car['fuel'],car['kms'],car['doors'],
            #               car['emissions'],car['seller'],car['color'],car['warranty'],car['year'],
            #               car['trunk'],car['length'],car['height'],car['width'],car['weight'],car['seats'],car['deposit_size'],
            #               car['body_style'],car['max_speed'],car['combined_consume'],car['urban_consume'],
            #               car['rural_consume'],car['acceleration0to100'],car['transmission'],car['autonomy'],car['cylinders'],
            #               car['displacement'],car['speeds'],car['max_pairs'],car['drive_position'],car['description'],car['equipment'],car['link'],))


            self.conn.commit()
            self.conn.close()

        except Exception as e:
            print(str(e))
            pass

    def selectFullCarByLink(self, link):
        carItem = None
        try:

            self.conn = self.connect(self.DATABASE_FILENAME, sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES)
            c = self.conn.cursor()

            t = (link,)
            c.execute('SELECT * FROM fullcars WHERE link=?', t)
            obj = c.fetchone()
            if obj is not None and obj is not "NoneType":
                carItem = self.convertTupleToFullCar(obj)
            self.conn.close()

        except Exception as e:
            print(str(e))
            pass

        return carItem

    def selectFullCarBySuperID(self, superID):
        carItem = None
        try:

            self.conn = self.connect(self.DATABASE_FILENAME, sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES)
            c = self.conn.cursor()

            t = (superID,)
            c.execute('SELECT * FROM fullcars WHERE superId=?', t)
            if c.fetchone() is not None:
                carItem = self.convertTupleToFullCar(c.fetchone())
            self.conn.close()

        except Exception as e:
            print(str(e))
            pass

        return carItem

    def selectSimpleCarByLink(self, link):
        carItem = None
        try:

            self.conn = self.connect(self.DATABASE_FILENAME, sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES)
            c = self.conn.cursor()

            t = (link,)
            c.execute('SELECT * FROM simplecars WHERE link=?', t)
            carItem = self.convertTupleToSimpleCar(c.fetchone())
            self.conn.close()

        except Exception as e:
            print(str(e))
            pass

        return carItem

    def selectSimpleCarALL(self):
        cars = []
        try:

            self.conn = self.connect(self.DATABASE_FILENAME, sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES)
            c = self.conn.cursor()

            c.execute('SELECT * FROM simplecars')
            alltuples = c.fetchall()
            for tuple in alltuples:
                cars.append(self.convertTupleToSimpleCar(tuple))
            self.conn.close()

        except Exception as e:
            print(str(e))
            pass

        return cars

    def convertTupleToFullCar(self, ntuple):
        car = CarItem()
        car.setCar(ntuple[1], ntuple[2], ntuple[38])
        car['brand'] = ntuple[3]
        car['sbrand'] = ntuple[4]
        car['model'] = ntuple[5]
        car['price'] = ntuple[6]
        car['fuel'] = ntuple[7]
        car['power'] = ntuple[8]
        car['city'] = ntuple[9]
        car['kms'] = ntuple[10]
        car['doors'] = ntuple[11]
        car['emissions'] = ntuple[12]
        car['seller'] = ntuple[13]
        car['color'] = ntuple[14]
        car['warranty'] = ntuple[15]
        car['year'] = ntuple[16]
        car['trunk'] = ntuple[17]
        car['length'] = ntuple[18]
        car['height'] = ntuple[19]
        car['width'] = ntuple[20]
        car['seats'] = ntuple[21]
        car['deposit_size'] = ntuple[22]
        car['weight'] = ntuple[23]
        car['body_style'] = ntuple[24]
        car['max_speed'] = ntuple[25]
        car['combined_consume'] = ntuple[26]
        car['urban_consume'] = ntuple[27]
        car['rural_consume'] = ntuple[28]
        car['acceleration0to100'] = ntuple[29]
        car['autonomy'] = ntuple[30]
        car['cylinders'] = ntuple[31]
        car['displacement'] = ntuple[32]
        car['max_pairs'] = ntuple[33]
        car['speeds'] = ntuple[34]
        car['transmission'] = ntuple[35]
        car['drive_position'] = ntuple[36]
        car['description'] = ntuple[37]
        car['equipment'] = ntuple[38]
        car['link'] = ntuple[39]
        return car

    def convertTupleToSimpleCar(self, ntuple):
        car = CarItem()
        car.setCar(ntuple[1], ntuple[2], ntuple[3])
        return car

    # def updateFullCarBySuperID(self, superID, car):
    #
    #     try:
    #
    #         self.conn = self.connect(self.filename, sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES)
    #         c = self.conn.cursor()
    #
    #         #c.execute("""UPDATE cars SET price = ? WHERE superID = ?""", (str(car['price']), superID))
    #         c.execute("""UPDATE fullcars SET price = '20.000E' WHERE carID = '1'""")
    #
    #         # c.execute("""UPDATE cars SET pageID= ? ,carID= ?,link= ?,brand= ?,sbrand= ?,model= ?,price= ?,fuel= ?,power= ?,city= ?,kms= ?,doors= ?,
    #         #                     emissions= ?,seller= ?,color= ?,warranty= ?,year= ?,trunk= ?,length= ?,height= ?,width= ?,seats= ?,deposit_size= ?,
    #         #                     weight=?,body_style=?,max_speed=?,combined_consume=?,rural_consume=?,urban_consume=?,acceleration0to100=?,autonomy=?,
    #         #                     cylinders=?,displacement=?,max_pairs=?,speeds=?,transmission=?,drive_position=?,description=? WHERE superID = ? """,
    #         #                     (car['pageID'], car['carID'], car['link'], car['brand'], car['sbrand'], car['model'], car['price'], car['fuel'], car['power'], car['city'], car['kms'], car['doors'],
    #         #                      car['emissions'], car['seller'], car['color'], car['warranty'], car['year'],car['trunk'], car['length'], car['height'], car['width'], car['seats'], car['deposit_size'],
    #         #                      car['weight'], car['body_style'], car['max_speed'], car['combined_consume'], car['rural_consume'], car['urban_consume'], car['acceleration0to100'], car['autonomy'],
    #         #                      car['cylinders'], car['displacement'], car['max_pairs'], car['speeds'], car['transmission'], car['drive_position'], car['description'], superID))
    #
    #         if c.rowcount > 0:
    #             car = self.selectBySuperID(superID)
    #         else:
    #             car = None
    #         self.conn.close()
    #
    #     except Exception as e:
    #         print(str(e))
    #         car = None
    #         pass
    #
    #     return car


